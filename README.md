## Some useful LXD tools.

Package provides cli tools for [LXD](https://linuxcontainers.org/lxd/introduction/) container virtualization manager. 


### Purpose
LXD has a powerful cli abilities. But, some of common operations, that you
 may want to execute, are not available throught it`s cli. So, this package
  extends LXD cli abilities.


### Installation (standalone usage)
```bash
sudo -H python3 -m pip install -U pip
sudo -H python3 -m pip install lxd_tools
```
*Updating pip is required for correct `cryptography` requirement installation.*


### Usage
There will be those scripts available after package installation:
 - `lxd_print_resources`
 
In order to display help message, you should use `--help` flag with any
 script, that you needed. For example:
```bash
lxd_print_resources --help
```


### Compatibility

This package is python 3.7 and higher compatible.
